import styled from "styled-components";
import Logo from "../../assets/prancingPony.png";

export const Section = styled.section`
  width: 100%;
  height: 100%;
  background: linear-gradient(
      to bottom,
      rgba(41, 41, 29, 0.5) 0%,
      rgba(41, 41, 29, 0.5) 100%
    ),
    url(${Logo});
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
`;

export const Title = styled.h1`
  width: 100%;
  color: white;
  position: absolute;
  top: 5%;
  right: 50%;
  transform: translate(50%, -50%);
  width: 272px;
  font-size: 38px;
  @media (min-width: 800px) {
    top: 40%;
  }
`;

export const FlexContainer = styled.div`
  display: grid;
  @media (min-width: 800px) {
    display: flex;
    width: 100%;
  }
`;

export const Container = styled.div`
  width: 100%;
  height: 33vh;
  display: flex;
  align-items: center;
  justify-content: center;
  button {
    width: 200px;
    height: 50px;
    font-size: 18px;
    background-color: var(--blue);
    color: white;
  }
  @media (min-width: 800px) {
    height: 100vh;
  }
`;
