import { useContext } from "react";
import { useHistory } from "react-router";
import Button from "../../components/Button";
import { CatalogueContext } from "../../providers/catalogue";
import { Section, Container, FlexContainer, Title } from "./styles";

const Dashboard = () => {
  let history = useHistory();
  const { occasions } = useContext(CatalogueContext);

  const handleClick = (selected) => {
    history.push(`/${selected}/1`);
  };
  return (
    <>
      <Section>
        <Title>Qual a ocasião?</Title>
        <FlexContainer>
          {occasions.map((occasion) => {
            return (
              <Container>
                <Button onClickFunc={() => handleClick(occasion.name)}>
                  {occasion.translation}
                </Button>
              </Container>
            );
          })}
        </FlexContainer>
      </Section>
    </>
  );
};

export default Dashboard;
