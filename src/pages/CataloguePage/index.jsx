import CatalogueList from "../../components/CatalogueList";
import PageHandler from "../../components/PageHandler";
import { Container } from "./styles";
import Nav from "../../components/Nav";

const CataloguePage = () => {
  return (
    <Container>
      <Nav />
      <PageHandler></PageHandler>
      <CatalogueList></CatalogueList>
      <PageHandler></PageHandler>
    </Container>
  );
};

export default CataloguePage;
