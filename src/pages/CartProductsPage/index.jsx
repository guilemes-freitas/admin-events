import { Container } from "./styles";
import Nav from "../../components/Nav";
import Cart from "../../components/Cart";

const CartProductsPage = () => {
  return (
    <Container>
      <Nav />
      <Cart></Cart>
    </Container>
  );
};

export default CartProductsPage;
