import { createContext, useState } from "react";
import api from "../../services/api";

export const CatalogueContext = createContext([]);

export const CatalogueProvider = ({ children }) => {
  const [catalogue, setCatalogue] = useState([]);
  const [nextPage, setNextPage] = useState(true);
  const occasions = [
    {
      name: "Wedding",
      translation: "Casamento",
    },
    {
      name: "Confraternization",
      translation: "Confraternização",
    },
    {
      name: "Graduation",
      translation: "Graduação",
    },
  ];

  const changePage = (page) => {
    const futurePage = parseInt(page) + 1;
    api.get(`beers?page=${page}`).then((response) => {
      setCatalogue(response.data);
    });

    api.get(`beers?page=${futurePage}`).then((response) => {
      setNextPage(!!response.data.length);
    });
  };

  return (
    <CatalogueContext.Provider
      value={{ catalogue, nextPage, occasions, changePage }}
    >
      {children}
    </CatalogueContext.Provider>
  );
};
