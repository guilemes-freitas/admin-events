import { CatalogueProvider } from "./catalogue";
import { CartProvider } from "./cart";

const Providers = ({ children }) => {
  return (
    <CatalogueProvider>
      <CartProvider>{children}</CartProvider>
    </CatalogueProvider>
  );
};

export default Providers;
