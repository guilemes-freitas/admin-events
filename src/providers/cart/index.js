import { createContext, useState } from "react";
import { useParams } from "react-router";
export const CartContext = createContext([]);

export const CartProvider = ({ children }) => {
  const { occasion } = useParams();
  const initialState =
    JSON.parse(localStorage.getItem(`@${occasion}:Cart`)) || [];
  const [cart, setCart] = useState(initialState);

  const addToCart = (occasion, product) => {
    const list = JSON.parse(localStorage.getItem(`@${occasion}:Cart`)) || [];
    product && list.push(product);
    localStorage.setItem(`@${occasion}:Cart`, JSON.stringify(list));
    setCart(list);
  };

  const removeFromCart = (product, occasion) => {
    const list = cart.filter((filtered) => filtered.name !== product.name);
    localStorage.setItem(`@${occasion}:Cart`, JSON.stringify(list));
    setCart(list);
  };

  const updateCart = (occasion) => {
    console.log(occasion);
    const list = JSON.parse(localStorage.getItem(`@${occasion}:Cart`));
    console.log(list);
    setCart(list);
  };

  return (
    <CartContext.Provider
      value={{ cart, addToCart, removeFromCart, updateCart }}
    >
      {children}
    </CartContext.Provider>
  );
};
