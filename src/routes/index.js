import { Route, Switch } from "react-router";
import CartProductsPage from "../pages/CartProductsPage";
import CataloguePage from "../pages/CataloguePage";
import Dashboard from "../pages/Dashboard";

const Routes = () => {
  return (
    <Switch>
      <Route path="/:occasion/cart">
        <CartProductsPage></CartProductsPage>
      </Route>
      <Route path="/:occasion/:page">
        <CataloguePage></CataloguePage>
      </Route>
      <Route exact path="/">
        <Dashboard></Dashboard>
      </Route>
    </Switch>
  );
};

export default Routes;
