import { useContext } from "react";
import { useHistory, useParams } from "react-router";
import Button from "../../components/Button";
import { CatalogueContext } from "../../providers/catalogue";
import { Container } from "./styles";

const PageHandler = () => {
  let history = useHistory();
  const { nextPage, changePage } = useContext(CatalogueContext);
  const { page, occasion } = useParams();

  const handleClick = (goTo) => {
    changePage(goTo);
    history.push(`/${occasion}/${goTo}`);
  };

  return (
    <Container>
      {page > 1 && (
        <Button onClickFunc={() => handleClick(parseInt(page) - 1)}>
          Voltar
        </Button>
      )}
      {nextPage && (
        <Button onClickFunc={() => handleClick(parseInt(page) + 1)}>
          Próxima
        </Button>
      )}
    </Container>
  );
};

export default PageHandler;
