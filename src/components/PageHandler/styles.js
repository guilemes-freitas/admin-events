import styled from "styled-components";

export const Container = styled.section`
  display: flex;
  justify-content: center;
  width: 100%;
  margin: 2rem 0;
  gap: 0.5rem;
  button {
    border-radius: 4px 0 0 4px;
  }
  button + button {
    border-radius: 0 4px 4px 0;
  }
`;
