import Button from "../Button";
import { Container, Image, ProductName } from "./styles";

const Product = ({ product, onClickFunc, removable }) => {
  return (
    <Container>
      <ProductName>{product.name}</ProductName>
      <Image backgroundImage={product.image_url}></Image>
      <span>First brewed: {product.first_brewed}</span>
      <span>
        {product.boil_volume.unit} : {product.boil_volume.value}
      </span>

      <Button onClickFunc={onClickFunc}>
        {removable ? "Remover" : "Comprar"}
      </Button>
    </Container>
  );
};

export default Product;
