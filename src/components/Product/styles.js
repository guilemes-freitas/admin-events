import styled from "styled-components";
import Logo from "../../assets/prancingPony.png";

export const Container = styled.div`
  width: 40%;
  height: 275px;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
  position: relative;
  text-align: center;

  color: var(--white);
  background: var(--green);
  border-radius: 4px;
  box-shadow: 0px 3px 3px rgb(0 0 0 / 30%);

  @media (min-width: 800px) {
    width: 250px;
    height: 325px;
  }
`;

export const Image = styled.div`
  width: 50%;
  height: 125px;
  background-image: url(${(props) =>
    props.backgroundImage ? props.backgroundImage : Logo});
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
  @media (min-width: 800px) {
    height: 175px;
  }
`;

export const ProductName = styled.h4`
  width: 90%;
  font-size: 12px;
  @media (min-width: 800px) {
    font-size: 16px;
  }
`;
