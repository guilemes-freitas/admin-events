import { useContext } from "react";
import { useParams } from "react-router";
import { CartContext } from "../../providers/cart";
import { CatalogueContext } from "../../providers/catalogue";
import Product from "../Product";
import { Container, CartContainer, CartInfo } from "./styles";

const Cart = () => {
  const { cart, removeFromCart, addToCart } = useContext(CartContext);
  const { occasions } = useContext(CatalogueContext);
  const { occasion } = useParams();
  const selected = occasions.filter((item) => {
    return item.name === occasion;
  });

  const handleClick = (item) => {
    removeFromCart(item, occasion);
  };

  return (
    <Container>
      <CartInfo>
        <h1>Carrinho de {selected[0].translation}</h1>
      </CartInfo>
      <CartContainer>
        {cart.map((product) => (
          <Product
            key={product.id}
            product={product}
            removable
            onClickFunc={() => {
              handleClick(product);
            }}
          />
        ))}
      </CartContainer>
    </Container>
  );
};

export default Cart;
