import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const CartContainer = styled.div`
  width: 80%;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  gap: 0.5rem;
  > div {
    display: flex;
    width: 200px;
    height: 320px;
    font-size: 12px;
  }
  > div img {
    height: 220px;
  }
`;

export const CartInfo = styled.div`
  text-align: center;
  color: var(--white);
  margin: 3rem;
`;
