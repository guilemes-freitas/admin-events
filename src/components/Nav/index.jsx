import { Link, useHistory, useParams } from "react-router-dom";
import Logo from "../../assets/prancingPony.png";
import { Container, NavContainer, HomeLink } from "./styles";

import { FiShoppingCart } from "react-icons/fi";

import { NavLink } from "./styles";
import { Badge } from "@material-ui/core";
import { CartContext } from "../../providers/cart";
import { useContext } from "react";
import { useEffect } from "react";
import { useState } from "react";
import { CatalogueContext } from "../../providers/catalogue";

export default function PrimarySearchAppBar() {
  const { cart, addToCart } = useContext(CartContext);
  const { occasion } = useParams();
  const { occasions } = useContext(CatalogueContext);
  const history = useHistory();
  const [alreadyLoaded, setAlreadyLoaded] = useState(false);

  useEffect(() => {
    const check = occasions.map((item) => {
      return item.name;
    });
    !check.includes(occasion) && history.push("/");
    if (!alreadyLoaded) {
      addToCart(occasion);
      setAlreadyLoaded(true);
      // console.log(cart);
    }
  });
  return (
    <Container>
      <NavContainer>
        <Link to="/">
          <HomeLink>
            <img src={Logo} alt="logo"></img>
          </HomeLink>
        </Link>
      </NavContainer>
      <Link to={`/${occasion}/1`}>
        <h3> Página inicial </h3>
      </Link>
      <nav>
        <NavLink to={`/${occasion}/cart`}>
          <Badge badgeContent={cart.length} color="primary">
            <FiShoppingCart size={20} />
          </Badge>
          <span> Carrinho </span>
        </NavLink>
      </nav>
    </Container>
  );
}
