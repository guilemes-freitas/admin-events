import { Link } from "react-router-dom";
import styled from "styled-components";

export const NavLink = styled(Link)`
  margin: 10px;
  font-size: 18px;

  color: var(--white);

  > svg {
    transform: translateY(3.5px);
    margin-right: 10px;
  }

  span {
    margin-left: 5px;
  }
`;

export const Container = styled.header`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: space-between;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  margin-bottom: 0.5rem;
  background-color: var(--green);
  width: 100%;
  a {
    text-decoration: none;
    color: var(--white);
  }
`;

export const NavContainer = styled.nav`
  display: flex;
  align-items: center;
  justify-content: space-around;
  a {
    padding-left: 1rem;
  }
  img {
    width: 100px;
  }
`;

export const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 150px;
  margin-right: 1rem;
  button {
    margin-top: 0;
  }
`;

export const HomeLink = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
