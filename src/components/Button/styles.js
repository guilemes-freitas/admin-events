import styled from "styled-components";

export const Container = styled.button`
  width: 75px;
  background-color: var(--orange);
  color: var(--green);
  border: none;
  height: 25px;
  border-radius: 4px;
  font-weight: bold;
`;
