import { Container } from "./styles";

const Button = ({ onClickFunc, children }) => {
  return (
    <Container onClick={onClickFunc}>
      <p>{children}</p>
    </Container>
  );
};

export default Button;
