import { useContext, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { CatalogueContext } from "../../providers/catalogue";
import { CartContext } from "../../providers/cart";
import Product from "../Product";
import { Container } from "./styles";

const CatalogueList = () => {
  const history = useHistory();
  const [alreadyLoaded, setAlreadyLoaded] = useState(false);
  const { page, occasion } = useParams();
  const { catalogue, changePage, occasions } = useContext(CatalogueContext);
  const { addToCart } = useContext(CartContext);

  const handleClick = (item) => {
    addToCart(occasion, item);
  };

  useEffect(() => {
    const check = occasions.map((item) => {
      return item.name;
    });
    !check.includes(occasion) && history.push("/");
    if (!alreadyLoaded) {
      changePage(page);
      setAlreadyLoaded(true);
    }
  });

  return (
    <Container>
      {catalogue.map((product) => {
        return (
          <Product
            key={product.id}
            product={product}
            onClickFunc={() => {
              handleClick(product);
            }}
          />
        );
      })}
    </Container>
  );
};

export default CatalogueList;
