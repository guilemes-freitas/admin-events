import styled from "styled-components";

export const Container = styled.section`
  width: 95%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 0.5rem;
  @media (min-width: 800px) {
    gap: 1.5rem;
  }
`;
