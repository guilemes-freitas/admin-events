import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: 0;
    }

    :root {
        --blue: #1480fb;
        --indigo: #007aff;
        --purple: #01356c;
        --white: #cfc5b1;
        --gray: #f0f0f0;
        --darkBlue: #05133d;
        --red: #c53030;
        --green: #29291d;
        --light-green: #5c563c;
        --orange: #c1a872;
    }

    body{
        background: var(--light-green);
        font-family: 'MedievalSharp', cursive;
    }

`;
